/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 * @brief The SerialReaderThread interacts with the serial port to read ***
 * 
 */
#ifndef SERIALREADERTHREAD_H
#define SERIALREADERTHREAD_H

#include <unistd.h>
#include <QThread>
#include <QtSerialPort/QSerialPort>
#include <QDateTime>
#include <iostream>

#include "math.h"

extern bool SLEEP_DISABLED;


struct mmwave_header_s
{
    char magicword[8];
    char sdk_version[4];
    char packet_length[4];
    char platform[4];
    char frame_no[4];
    char cpu_time[4];
    char num_detected_obj[4];
    char tlv_no[4];
    char subframe_no[4];
    // char pad[8];

} __attribute__((packed));

enum mmwave_tlv_types_e
{
    MMWDEMO_OUTPUT_MSG_DETECTED_POINTS                      = 1,
    MMWDEMO_OUTPUT_MSG_RANGE_PROFILE                        = 2,
    MMWDEMO_OUTPUT_MSG_NOISE_PROFILE                        = 3,
    MMWDEMO_OUTPUT_MSG_AZIMUT_STATIC_HEAT_MAP               = 4,
    MMWDEMO_OUTPUT_MSG_RANGE_DOPPLER_HEAT_MAP               = 5,
    MMWDEMO_OUTPUT_MSG_STATS                                = 6,
    MMWDEMO_OUTPUT_MSG_DETECTED_POINTS_SIDE_INFO            = 7,
    MMWDEMO_OUTPUT_MSG_AZIMUT_ELEVATION_STATIC_HEAT_MAP     = 8,
    MMWDEMO_OUTPUT_MSG_TEMPERATURE_STATS                    = 9,
    MMWDEMO_OUTPUT_EXT_MSG_RANGE_PROFILE_MAJOR              = 302,
    MMWDEMO_OUTPUT_EXT_MSG_RANGE_PROFILE_MINOR              = 303,
    MMWDEMO_OUTPUT_MSG_SPHERICAL_POINTS                     = 1000,
    MMWDEMO_OUTPUT_MSG_TRACKERPROC_3D_TARGET_LIST           = 1010,
    MMWDEMO_OUTPUT_MSG_TRACKERPROC_TARGET_INDEX             = 1011,
    MMWDEMO_OUTPUT_MSG_COMPRESSED_POINTS                    = 1020,
    MMWDEMO_OUTPUT_MSG_OCCUPANCY_STATE_MACHINE              = 1030,
    MMWDEMO_OUTPUT_MSG_VITALSIGNS                           = 1040,
    MMWDEMO_OUTPUT_MSG_ENHANCED_PRESENCE_DETECTED           = 2000
};

typedef struct mmwave_point
{
    float x;
    float y;
    float z;
    float doppler;
} __attribute__((packed)) mmwave_point_t;


Q_DECLARE_METATYPE(std::vector<mmwave_point_t>);


class SerialReaderThread : public QThread
{
    Q_OBJECT

public:
    SerialReaderThread();
    ~SerialReaderThread(void);
    void setSerialPortName(QString port_name);
    bool parseData(QByteArray bytes, mmwave_header_s header);


signals:
    void srtQuit();
    void sendActiveState(bool active);
    void sendPoints(std::vector<mmwave_point_t>, int);

public slots:
    void handleQuit();

protected:
    void run();
    void handleTLV(int tlv_value, int tlv_len, QByteArray ba);


private:
    int createSerialPort();
    QSerialPort *m_serialPort;
    bool m_runSerialThread;
    QString m_serialPortName;
};
#endif // SERIALREADERTHREAD_H

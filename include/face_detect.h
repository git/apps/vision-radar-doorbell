/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#ifndef FACEDETECTION_H
#define FACEDETECTION_H

#include <iostream>

#include <QObject>
#include <QDebug>
#include <QVideoFilterRunnable>
#include <QAbstractVideoFilter>
#include <QImage>
#include <QDateTime>

#include <sys/time.h>

#include "opencv2/opencv.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/imgproc.hpp"

#include "serialreaderthread.h"

extern bool FLIP_IMAGE; // set initially from CLI, do not update
extern int NUM_PERSISTENCE_POINT_CLOUDS; // set initially, do not update
extern bool DISPLAY_POINTS;
extern bool RUN_AI;


/**
 * @brief The FaceDetectFilterRunnable class is what runs people detection...
 * 
 */
class FaceDetectFilterRunnable : public QVideoFilterRunnable
{
	// Q_OBJECT
private:
	bool doFaceDetection = false;
	qint64 timeStartFaceDetectMsec;
	cv::CascadeClassifier cascade;

	QAbstractVideoFilter* filter;
	// std::vector<mmwave_point_t> pointCloud, previousPointCloud;
	std::deque<std::vector<mmwave_point_t>> pointClouds;
public:
	FaceDetectFilterRunnable(QAbstractVideoFilter* creator);
	QVideoFrame run(QVideoFrame* input, const QVideoSurfaceFormat& surfaceFormat, RunFlags flags);

	std::vector<cv::Rect> detectFaces(cv::Mat rgb_image, double scale_x, double scale_y);
	void processDetectedFace(cv::Mat image, cv::Rect face, double scale_x, double scale_y);
	cv::Mat draw3DPointsOnImage(std::vector<mmwave_point_t> points, cv::Mat image);

	void startDetecting(bool enable);
	void updatePointCloud(std::vector<mmwave_point_t>, int);
	bool isDetecting();

};


/**
 * @brief The FaceDetectFilter class modifies video inputs and produces video outputs frame by frame. This filter is paired with FaceDetectFilterRunnable, which performs people detection on each image and draws a box around each detected people. 
 * 
 * 		This filter is retrievable directly from the GUI, but the runnable within it is not, so it is used as a proxy for getters and setters. All image processing is handled within the attached runnable
 * 
 */
class FaceDetectFilter : public QAbstractVideoFilter
{
	Q_OBJECT

public:
	QVideoFilterRunnable* createFilterRunnable();

	void setRunFaceDetection(bool enable);
	void setParentQObject(QObject* object);
	bool isRunningFaceDetect();
	void updateFPSCounter(int fps);
	void setPoints(std::vector<mmwave_point_t>, int);
	std::vector<cv::Rect> detectFaces(cv::Mat rgb_image, double scale_x, double scale_y);

private: 
	QObject* object;
	QObject* fpsTextObject;
	QObject* powerReadingsObject;
	FaceDetectFilterRunnable* faceDetectFilterRunnable = NULL;


signals:
	void faceDetected();

public slots:
	void receiveNewActiveState(bool enable);
	void receivePointCloud(std::vector<mmwave_point_t>, int);


};

#endif //FACEDETECTION_H
/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#include <QGuiApplication>
#include <QQmlEngine>
#include <qqml.h>
#include <QQmlComponent>
#include <QtSerialPort/QSerialPort>
#include <QDebug>
#include <QQmlContext>
#include <QScreen>

#include "serialreaderthread.h"
#include "face_detect.h"


int main(int argc, char *argv[])
{
    qDebug() << "Open main";

    QScreen *screen;
    QRect screenGeometry;
    FaceDetectFilter* faceDetectFilter;
    SerialReaderThread *radarReaderThread;
    int screen_height, screen_width;

    /* Register types that are needed in QML files or for signal-slot communications */
    qmlRegisterType<FaceDetectFilter>("com.tihmi.classes", 1, 0, "FaceDetectFilter");
    qRegisterMetaType<std::vector<mmwave_point_t>>();

    /* Setup the base elements of the application, including loading in the QML file describing the GUI */
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QQmlEngine engine;
    engine.clearComponentCache();
    QQmlComponent component(&engine, QUrl("qrc:/ui/main.qml"));
    QObject *mainObject = component.create();

    printf("Usage: mmwave_doorbell -platform eglfs\n");
    if(mainObject == NULL){
        qDebug() << "Cannot create root object\n";
        qDebug() << component.errors();
        return -1;
    }


    /* Set the main window property like image name, displayed width and height of the image */
    screen = QGuiApplication::primaryScreen();
    screenGeometry = screen->geometry();
    screen_width = screenGeometry.width();
    screen_height = screenGeometry.height();
    mainObject->setProperty("width", screen_width);
    mainObject->setProperty("height", screen_height);


    /* Create objects that interact with the GUI and inputs at an application level. Those that interact with the GUI use the main object for modifying GUI parameters/elements */
    qDebug() << "Creating objects and QT connections";
    
    /* Read information from the command line */
    radarReaderThread = new SerialReaderThread();

    if (argc >= 2)
    {
        int current_arg = 1;
        while (current_arg < argc)
        {
            if ( std::strncmp(argv[current_arg], "-s", 2) == 0 && current_arg+1 < argc) {
                //get serial port name
                radarReaderThread->setSerialPortName(argv[current_arg+1]);
                current_arg+=2;
            } 
            else if (std::strncmp(argv[current_arg], "-h", 2) == 0 ) {
                qDebug() << "Usage: mmwave_doorbell -s /path/to/serial/port -f -p NUM_PERSISTENT_POINTCLOUDS\n  -s: serial port filepath\n  -f: flip image along x axis (up-down) for display\n  -p: number of consecutive point clouds to use when drawing to the display (default 5)\n  --no-pointcloud: turn off point cloud display";
                if (argc == 2) exit(1);
            }
            else if (std::strncmp(argv[current_arg], "-f", 2) == 0 ) {
                FLIP_IMAGE = true;
                current_arg++;
            }
            else if (std::strncmp(argv[current_arg], "-p", 2) == 0 && current_arg+1 < argc)
            {
                NUM_PERSISTENCE_POINT_CLOUDS = atoi(argv[current_arg+1]);
                current_arg+=2;
            }
            else if (std::strncmp(argv[current_arg], "--no-pointcloud", 15) == 0) {
                DISPLAY_POINTS = false;
                mainObject->setProperty("displaypoints", false);
                current_arg++;
            }
            else if (std::strncmp(argv[current_arg], "--no-ai", 15) == 0) {
                RUN_AI = false;
                current_arg++;
            }
            else if (std::strncmp(argv[current_arg], "--no-sleep", 10) == 0)
            {
                SLEEP_DISABLED = true;
                qDebug() << "Sleep disabled" << SLEEP_DISABLED;
                mainObject->setProperty("activescreen", true);
                current_arg++;
            }


            else {
                qDebug() << "Unrecognized argument '" << argv[current_arg] << "'; skipping..";

                current_arg++;
            }
        }
    }
    
    /* Create objects that interact with the GUI and inputs at an application level. Those that interact with the GUI use the main object for modifying GUI parameters/elements */
    faceDetectFilter = mainObject->findChild<FaceDetectFilter*>("faceDetectFilter");
    faceDetectFilter->setParentQObject(mainObject);

    qDebug() << "Creating QT Object connections";
    /*Connect the signals and slots sensor-reading threads, HMI logic thread, and GUI-controlling Keyword object */
    //Read activity over serial from mmWave sensor to HMI logic
    QObject::connect(radarReaderThread, &SerialReaderThread::sendActiveState, faceDetectFilter, &FaceDetectFilter::receiveNewActiveState, Qt::QueuedConnection);
    QObject::connect(radarReaderThread, &SerialReaderThread::sendPoints, faceDetectFilter, &FaceDetectFilter::receivePointCloud, Qt::QueuedConnection);
   

    //Cleanup
    QObject::connect(radarReaderThread, &SerialReaderThread::srtQuit, &app, &QGuiApplication::quit, Qt::QueuedConnection);

    /* Start thread that reads the gesture from mmWave Sensor and take predetermined action per the gestures received */
    
    radarReaderThread->start();
    qDebug() << "Starting without radar serial thread"; 

    qDebug() << "Starting application";

    app.exec();

    qDebug() << "QGuiApplication execution over\n";

    return 0;
}


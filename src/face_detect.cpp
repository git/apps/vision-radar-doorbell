/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#include "face_detect.h" 

const qint64 NULL_START_TIME = -1;

//input image dimensions. This needs to match what the camera is told to collect in the QML file
const int IMG_HEIGHT = 720;
const int IMG_WIDTH = 1280;

/* NB: Camera FOV, Focal length, and correction coeff are parameters of the camera lens. The sensor width and height are functions of the image sensor itself (e.g. OV5640) that is inside the camera. */
const double CAMERA_FOV_DEGREES = 84.0;
const double CAM_FOCAL_LEN_M = 0.0032; // from Technexion OV5640 module product brief
const double CAM_SENSOR_WIDTH = 0.00362;
const double CAM_SENSOR_HEIGHT = 0.00272;
const double CAM_LENS_DISTORTION_CORRECTION_COEFF = 0.0015; // magic number, tuned by trial and error

const double CAM_DISTANCE_RADAR_Z_M = 0.2; //positive means camera is above radar
const double CAM_DISTANCE_RADAR_Y_M = 0.1; //positive means camera is behind radar
const double CAM_DISTANCE_RADAR_X_M = 0.02; //positive means camera is to the right of the radar (from it's POV)

//Configured in radar cfg
const int RADAR_MIN_AZI_ANGLE_DEG = -57;
const int RADAR_MAX_AZI_ANGLE_DEG = 55;
const int RADAR_MIN_ELE_ANGLE_DEG = -57;
const int RADAR_MAX_ELE_ANGLE_DEG = 56;
const double MIN_DISTANCE_R_M = 0.05;


const double ALPHA_RANGE_FILTER = 0.5;

bool FLIP_IMAGE = false;
bool DISPLAY_POINTS = true;
bool RUN_AI = true;
int NUM_PERSISTENCE_POINT_CLOUDS = 5; // set initially, do not update

const char* FACE_MODEL_PATH = "facemodels/haarcascade_frontalface_default.xml";
const int FACE_IMAGE_EXTRA_PX = 50;



/** TODO: document me!
 * 
 */
cv::Scalar convertIDtoColor(int id){
    int divisions_per_channel = 3; // minimum 2: should come out to 0 and 255
    int r, g, b = 0;

    if (divisions_per_channel < 2) divisions_per_channel= 2;


    float stepsize = 255 / (divisions_per_channel - 1);

    r = (int)(stepsize * (id % divisions_per_channel));
    g = (int)(stepsize * ((id/2)  % divisions_per_channel));
    b = (int)(stepsize * ((id/4) % divisions_per_channel));

    return cv::Scalar((uint8_t) r, (uint8_t) g, (uint8_t) b);

}

/**
 * @brief Convert an image from QT format (QVideoFrame) to openCV's Mat format, specifically as an RGB image
 * 
 * @param input A QVideoFrame provided by the video filter. The format will depend on the camera being used. During development, a Logitech C270 camera was used, which provides images in YUYV format w/ 8-bits per channel. **The input must already be "mapped" for input->bits to return valid data**
 * @return cv::Mat RGB image with the same x,y dimensions as the input
 */
cv::Mat getRGBImageFromFrame(QVideoFrame* input)
{
    int input_h, input_w;
    uchar* image_data;
    cv::Mat rgb;

    input_h = input->height();
    input_w = input->width();

    image_data = input->bits(); //This should have already been 'mapped'

    if (input->pixelFormat() == QVideoFrame::Format_YUYV){
        /* Convert image into opencv format*/
        cv::Mat yuyv(input_h, input_w, CV_8UC2, image_data); 

        /* Convert image to RGB */
        rgb = cv::Mat(input_h, input_w, CV_8UC3);
        cv::cvtColor(yuyv, rgb, cv::COLOR_YUV2RGB_YUYV);
    }
    else if (input->pixelFormat() == QVideoFrame::Format_UYVY) {
        /* Convert image into opencv format*/
        cv::Mat yuyv(input_h, input_w, CV_8UC2, image_data); 

        /* Convert image to RGB */
        rgb = cv::Mat(input_h, input_w, CV_8UC3);
        cv::cvtColor(yuyv, rgb, cv::COLOR_YUV2RGB_UYVY);
    }
    else if (input->pixelFormat() == QVideoFrame::Format_RGB24)
    {
        rgb = cv::Mat(input_h, input_w, CV_8UC3, image_data);
    }
    else {
        qDebug() << "FaceDetectionFilter failed to read image due to unexpected format: " << input->pixelFormat();
    }

    return rgb;
}

/**
 * @brief Construct a new People Detect Filter Runnable to handle people detection on video inputs. This will load the cascade model used for people detection
 * 
 * @param creator The handle of the QAbstractVideoFilter that created this runnable. This should be castable to a FaceDetectFilter
 */
FaceDetectFilterRunnable::FaceDetectFilterRunnable(QAbstractVideoFilter* creator){
    filter = creator;
    this->cascade.load(FACE_MODEL_PATH);

    qDebug() << "created runnable";


}


/**
 * @brief Run face detection using conventional methods (cascade classifier)
 * 
 * @param rgb_image an RGB image containing faces to recognize 
 * @param scale_x A double representing the scale to resize the image by in x-dimension. <1 results in a smaller image, >1 in a larger one.
 * @param scale_y A double representing the scale to resize the image by in x-dimension
 * @return std::vector<cv::Rect> A vector of rectangles representing where faces were detected
 */
std::vector<cv::Rect> FaceDetectFilterRunnable::detectFaces(cv::Mat rgb_image, double scale_x, double scale_y)
{
    std::vector<cv::Rect> faces;
    cv::Mat gray_image, small_image;

    //classification parameters
    int num_rectangles_needed = 5, flags = 0x0;
    double scale_per_step = 1.3;
    cv::Size min_size(30,30);

    // convert to grayscale and resize based on scale factors
    cv::cvtColor(rgb_image, gray_image, cv::COLOR_RGB2GRAY);
    cv::resize(gray_image, small_image, cv::Size(), scale_x, scale_y, cv::INTER_LINEAR);
    // represent the image with a histogram before classication
    cv::equalizeHist(small_image, small_image);

    // run classification / face detection using the loaded cascade-model
    cascade.detectMultiScale(small_image, faces, scale_per_step,num_rectangles_needed , flags, min_size);

    return faces;
}


cv::Point pointToPixel_projection(mmwave_point_t point)
{
    double mag, angle_elevation, angle_azi;
    double x, y;
    cv::Point return_point = cv::Point(-1,-1);
    mag = sqrt(point.x * point.x + point.y * point.y + point.z * point.z);
    angle_elevation = asin(point.z / mag) * 180 / M_PI;
    angle_azi = atan(point.x / point.y) * 180 / M_PI;

    angle_azi *= -1; //image is flipped, so the azimuthal angle should be too
    point.x = point.x * -1;

    if (
        abs(angle_elevation) < CAMERA_FOV_DEGREES/2 && 
        abs(angle_azi) < CAMERA_FOV_DEGREES / 2 && 
        mag >= MIN_DISTANCE_R_M
    ) {
        x = ((CAM_FOCAL_LEN_M / point.y) * point.x) / CAM_SENSOR_WIDTH;
        x *= IMG_WIDTH/2;
        y = ((CAM_FOCAL_LEN_M / point.y) * point.z) / CAM_SENSOR_HEIGHT;
        y *= IMG_HEIGHT/2;
        // qDebug() << "(x,y) before LDC: " << x << "," << y;

        //Lens correction
        if (x < 0)
            x -= CAM_LENS_DISTORTION_CORRECTION_COEFF * x*x;
        else 
            x += CAM_LENS_DISTORTION_CORRECTION_COEFF * x*x;
        if (y < 0)
            y -= CAM_LENS_DISTORTION_CORRECTION_COEFF * y*y;
        else 
            y += CAM_LENS_DISTORTION_CORRECTION_COEFF * y*y;

        // qDebug() << "(x,y) after LDC: " << x << "," << y;


        //shift so that 0,0 in 3d is w/2,h/2
        x = IMG_WIDTH/2 + x; //0 is left of image
        y = IMG_HEIGHT/2 - y; //0 is top of image and increases downward in space


        return_point = cv::Point(x, y);
    }
    return return_point;
}


cv::Mat FaceDetectFilterRunnable::draw3DPointsOnImage(std::vector<mmwave_point_t> points, cv::Mat image)
{
    cv::Scalar white = cv::Scalar(255,255,255);
    cv::Scalar point_color = cv::Scalar(0x80,0x80,0x80);

    for (mmwave_point_t point : points) {
        double circle_scale;
        double mag, angle_elevation, angle_azi;
        cv::Point p;
        bool is_drawn = false;

        if (point.y <= 0.0) continue; // no distance from sensor. Invalid.

        mag = sqrt(point.x * point.x + point.y * point.y + point.z * point.z);

        circle_scale = (mag < 0.3) ? 1/0.3 : 1/mag;

        p = pointToPixel_projection(point);
        // qDebug() << "point from projection = (" << p_projection.x << "," << p_projection.y << ")";

        cv::circle(image, p, (int)25 * circle_scale, point_color,-1, 8,0); //negative thickness -> circle is filled
        cv::circle(image, p, (int)25 * circle_scale + 1, white,2, 8,0); //outer circle to improve visibilit

    }
    return image;

}

/**
 * @brief The top level function of the runnable filter, called on every new frame
 * 
 *      If people detection is enabled (using a setter function), this will convert the input image to RGB and do people detection with openCV. It will draw an people on the images shown in the live-feed in QML. After it has run for a few seconds, it will take a final image of the people to be cropped and displayed with a example visitor badge / ID card.
 * 
 *      This function can block the GUI from updating, so fast runtime is desireable
 * 
 * @param input The input image (QVideoFrame*)
 * @param surfaceFormat Not used
 * @param flags Not used
 * @return QVideoFrame 
 */
QVideoFrame FaceDetectFilterRunnable::run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags)
{
    QVideoFrame output;

    Q_UNUSED(flags); 
    Q_UNUSED(surfaceFormat);

    //default output should simply be the input
    output = *input; 

    // input->image().save("input image.png");

    //The image must be mapped to access the memory within; returns true on success
    bool image_mapped = input->map(QAbstractVideoBuffer::ReadOnly);

    if (image_mapped){
        cv::Mat rgb;        

        rgb = getRGBImageFromFrame(input);

        if (FLIP_IMAGE)
        {
            cv::rotate(rgb, rgb, cv::ROTATE_180);
        }

        if (!rgb.empty()) {
            /* Flip image along right/left axis to seem more like a mirror when displayed live*/
            cv::flip(rgb, rgb, 1);

            if (doFaceDetection && RUN_AI) {
                struct timespec start, end;
                double tdiff_ns;
                int fps;
                char time_str[16]; 
                std::vector<double> ranges;

                clock_gettime(CLOCK_MONOTONIC_RAW, &start);
                double scale_x = 0.4, scale_y = 0.4; //scale factors for the image before face detection. >0.5 results in noticeable latency in the GUI.
                std::vector<cv::Rect> faces = detectFaces(rgb, scale_x, scale_y);

                clock_gettime(CLOCK_MONOTONIC_RAW, &end);

                tdiff_ns = (end.tv_sec - start.tv_sec) * 1e9 + (end.tv_nsec - start.tv_nsec);
                sprintf(time_str, "%f", tdiff_ns/1e6);
                fps = (int) 1e9/tdiff_ns;
                qDebug() << "Face detection took [" << time_str << "] ms; fps is " << fps; 
                ((FaceDetectFilter*)(this->filter))->updateFPSCounter(fps);

                clock_gettime(CLOCK_MONOTONIC_RAW, &start);

                /* Draw a white box around the detected face */
                for (cv::Rect face : faces)
                {
                    cv::Rect rect(face.tl() / scale_x, face.br() / scale_y);
                    cv::rectangle(rgb, rect, cv::Scalar(255, 255, 255), 10);
                }


 
                //draw point clouds
                if (DISPLAY_POINTS) {
                    for (std::vector<mmwave_point_t> pointCloud : pointClouds)
                    {
                        rgb = draw3DPointsOnImage(pointCloud, rgb);
                    }
                }

                // clock_gettime(CLOCK_MONOTONIC_RAW, &end);
                // tdiff_ns = (end.tv_sec - start.tv_sec) * 1e9 + (end.tv_nsec - start.tv_nsec);
                // sprintf(time_str, "%f", tdiff_ns/1e6);
                // qDebug() << "Processing " << detected_persons.size() << "people took [" << time_str << "] ms"; 
            }


            /* Convert from opencv format back into QT image/video frame format*/
            QImage temp_img(rgb.data, rgb.cols, rgb.rows, QImage::Format_RGB888);
            temp_img.convertTo(QVideoFrame::imageFormatFromPixelFormat(QVideoFrame::Format_RGB32));
            output = QVideoFrame(temp_img);

            //unmap frees associated memory
            input->unmap();
            // output->image().save("output image.png");

        }
    }

    return output;
}

/** Getters and Setters **/
/**
 * @brief Control whether people detection should be running. If true, capture the current time so we can insert a delay between starting people detection and capturing an image for display
 * 
 * @param enable Whether to start (true) or stop (false) detecting people
 */
void FaceDetectFilterRunnable::startDetecting(bool enable) {
    doFaceDetection = enable; 
    // qDebug() << "Start detecting: " << enable;
    if (enable) timeStartFaceDetectMsec = QDateTime::currentMSecsSinceEpoch();
    else		timeStartFaceDetectMsec = NULL_START_TIME;
} 

/**
 * @brief Get a boolean representing whether people detection is running or not
 * 
 * @return true People Detection is running
 * @return false People Detection is not running
 */
bool FaceDetectFilterRunnable::isDetecting() {
    return doFaceDetection;
}

void FaceDetectFilterRunnable::updatePointCloud(std::vector<mmwave_point_t> points, int num_PCs)
{
    // qDebug() << num_PCs << "th frame from runnable";

    while (pointClouds.size() >= NUM_PERSISTENCE_POINT_CLOUDS)
    {
        pointClouds.pop_front();
    }
    pointClouds.push_back(points);

}


/**
 * @brief Create the runnable for the FaceDetectFilter. By default, people detection is not running when this is created.
 * 
 * @return QVideoFilterRunnable* A FaceDetectFilterRunnable* that will run each time a frame is captured from the video input (typically a camera)
 */
QVideoFilterRunnable* FaceDetectFilter::createFilterRunnable()
{
    qDebug() << "Creating filter";
    faceDetectFilterRunnable = new FaceDetectFilterRunnable(this);
    bool start_detection_on_boot = SLEEP_DISABLED; 
    setRunFaceDetection(start_detection_on_boot); 
    return faceDetectFilterRunnable;
}

/**
 * @brief Enables or disables people detection within the runnable
 * 
 * @param enable 
 */
void FaceDetectFilter::setRunFaceDetection(bool enable) 
{ 
    if (faceDetectFilterRunnable)
        faceDetectFilterRunnable->startDetecting(enable); 
}

/**
 * TODO: document this
 */ 
void FaceDetectFilter::setPoints(std::vector<mmwave_point_t> points, int num_PCs)
{
    for (mmwave_point_t point : points) {
        char point_str[64];
        sprintf(point_str, "point in filter(x,y,z): (%0.5f, %0.5f, %0.5f)", point.x, point.y, point.z);
        // qDebug() << point_str;
    }
    if (faceDetectFilterRunnable)
        faceDetectFilterRunnable->updatePointCloud(points, num_PCs); 
}


/**
 * @brief returns whether people detection is running or not. Simply a proxy for the runnable's equivalent getter.
 * 
 */
bool FaceDetectFilter::isRunningFaceDetect() { 
    return faceDetectFilterRunnable->isDetecting(); 
}


void FaceDetectFilter::setParentQObject(QObject* object) {
    this->object = object;
    this->fpsTextObject = object->findChild<QObject*>("FPSText");
    this->powerReadingsObject = object->findChild<QObject*>("PowerReadingsText");
}

void FaceDetectFilter::updateFPSCounter(int fps) {
    char fpsText[32];
    sprintf(fpsText, "Face-detect Perf: %d fps", fps);
    fpsTextObject->setProperty("text", QVariant(fpsText));
}

/**
 * @brief Controls whether people detection is enabled or disabled. Effectively a proxy for the runnable's equivalent setter.
 * 
 * @param enable 
 */
void FaceDetectFilter::receiveNewActiveState(bool enable)
{
    char powerReadingsText[128];

    setRunFaceDetection(enable);
    object->setProperty("activescreen", enable);

    if (enable)
    {
        sprintf(powerReadingsText, "Mode:\tACTIVE\nRadar:\t\t 5 mW\nVision MPU: 2 W");
    }    
    else {
        sprintf(powerReadingsText, "Mode:\tSLEEP\nRadar:\t\t 5 mW\nVision MPU: 10 mW");
    }

    powerReadingsObject->setProperty("text", QVariant(powerReadingsText));
}

void FaceDetectFilter::receivePointCloud(std::vector<mmwave_point_t> points, int num_PCs)
{   
    setPoints(points, num_PCs);
}
/*
* Copyright (C) 2023 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#include <QDebug>
#include <termios.h>
#include "serialreaderthread.h"

// Offsets supplied in configuration options for radar
const double RADAR_OFFSET_Z_M = 1.5 + 0.04;
const double RADAR_OFFSET_Y_M = 0;
const double RADAR_OFFSET_X_M = 0;

bool SLEEP_DISABLED = false;

/**
 * @brief Print a string as hexadecimal; useful for debugging the serial interface
 * 
 * @param str 
 * @param size 
 */
void printStringAsHex(char* str, int size)
{

    int pos = 0;


    printf("Printing %d byte hex string:", size);
    while(pos < size)
    {
        if (pos % 16 == 0)
        {
            printf("\r\n0x: ");
        }
        else if (pos % 2 == 0)
        {
            printf("  ");
        }
        
        printf("%02x", (uint8_t) str[pos]);
        pos++;
    }
    printf("\r\n");
}

/**
 * @brief  Helper function for print formatting of strings of hex that are read through the QSerialPort
 * 
 * 
 */
void printQStringHex(QByteArray str)
{
    int size;
    char* str_data;

    size = str.size();
    str_data = str.data();

    printStringAsHex(str_data, size);
}

/**
 * @brief Convert the raw byte string from UART for a floating point into the proper type
 * 
 * @param raw_prob little-endian byte-wise representation of single prevision float
 * @return float single precision floating-point representation of confidence percentage
 */
float uartProbToFloat(char raw_prob[4])
{

    uint32_t temp;
    float prob;

    memcpy(&temp, raw_prob, 4);
    uint32_t float_as_int =  le32toh((temp));
    memcpy(&prob, &float_as_int, 4);
    
    return prob;
}

inline uint32_t charArrayToInt(char arr[4])
{
    uint value = arr[0] + \
        (arr[1] << 8) + \
        (arr[2] << 16) + \
        (arr[3] << 24) ;

    return value;
}

bool isValidTLVType(int tlv_type) {
    bool isValid = false;

    switch (tlv_type)
    {
        case MMWDEMO_OUTPUT_MSG_DETECTED_POINTS:
        case MMWDEMO_OUTPUT_MSG_RANGE_PROFILE:
        case MMWDEMO_OUTPUT_MSG_NOISE_PROFILE:
        case MMWDEMO_OUTPUT_MSG_AZIMUT_STATIC_HEAT_MAP:
        case MMWDEMO_OUTPUT_MSG_RANGE_DOPPLER_HEAT_MAP:
        case MMWDEMO_OUTPUT_MSG_STATS:
        case MMWDEMO_OUTPUT_MSG_DETECTED_POINTS_SIDE_INFO:
        case MMWDEMO_OUTPUT_MSG_AZIMUT_ELEVATION_STATIC_HEAT_MAP:
        case MMWDEMO_OUTPUT_MSG_TEMPERATURE_STATS:
        case MMWDEMO_OUTPUT_EXT_MSG_RANGE_PROFILE_MAJOR:
        case MMWDEMO_OUTPUT_EXT_MSG_RANGE_PROFILE_MINOR:
        case MMWDEMO_OUTPUT_MSG_SPHERICAL_POINTS:
        case MMWDEMO_OUTPUT_MSG_TRACKERPROC_3D_TARGET_LIST:
        case MMWDEMO_OUTPUT_MSG_TRACKERPROC_TARGET_INDEX:
        case MMWDEMO_OUTPUT_MSG_COMPRESSED_POINTS:
        case MMWDEMO_OUTPUT_MSG_OCCUPANCY_STATE_MACHINE:
        case MMWDEMO_OUTPUT_MSG_VITALSIGNS:
        case MMWDEMO_OUTPUT_MSG_ENHANCED_PRESENCE_DETECTED:
            isValid = true;
            break;
        default:
            isValid = false;
            break;
    }

    return isValid;

}

bool readUntilPreamble(QSerialPort* port)
{
    bool found_preamble = false;
    int index = 0, magic_index = 0;
    const int LEN_MAGIC_WORD = 8;
    char magic_word[LEN_MAGIC_WORD] = {0x02, 0x01, 0x04, 0x03, 0x06, 0x05, 0x08, 0x07};
    char byte[1];

    while (magic_index < LEN_MAGIC_WORD)
    {
        // qDebug() << "s";

        bool cond = false;
        cond |= port->bytesAvailable() > 0;
        if (!cond) 
        {
            cond &= port->waitForReadyRead(100);
            // qDebug() << "w1";
        }
        // qDebug() << "c";
        if (cond)
        // if (port->bytesAvailable() > 0 || port->waitForReadyRead(100))
        {
            if (port->bytesAvailable() > 0 && port->read(byte, 1) > 0) 
            // if (port->read(byte, 1) > 0) 
            {
                // qDebug() << "r";
                if (*byte == magic_word[magic_index])
                {
                    magic_index++;
                    // qDebug() << index << " : " << magic_index;
                }
                else {
                    magic_index = 0;
                }
                // qDebug() << magic_index;

            }
        } //else qDebug() << "w2";
    }
    found_preamble = (magic_index==LEN_MAGIC_WORD) ? true : false;
    // qDebug() << "Done searching for preamble; found=" << found_preamble;
    return found_preamble;
}


/**
 * @brief Construct a new Serial Reader Thread:: Serial Reader Thread object
 * 
 * @param onlyProximity This sensor is only used to report proximity. It does not report gesture information -- this is for a dual-sensor setup where one is for proximity and one is for gesture detection.
 */
SerialReaderThread::SerialReaderThread()
{
    m_runSerialThread = true;
    m_serialPortName = "/dev/ttyACM0";

}

/**
 * @brief Destroy the Serial Reader Thread:: Serial Reader Thread object. Releases the serial port
 * 
 */
SerialReaderThread::~SerialReaderThread(void)
{
    qDebug() << "SerialReaderThread object is being deleted\n";
    m_serialPort->close();
    delete m_serialPort;
}

/**
 * @brief Set the name of the serial port to access. This is most often "/dev/ttyACM0" on linux machines
 * 
 * @param port_name 
 */
void SerialReaderThread::setSerialPortName(QString port_name)
{
    qDebug() << "Update serial port to: " << port_name;
    m_serialPortName = port_name;
}

/**
 * @brief Create the serial port to the mmwave radar sensor's data port.
 * 
 * @return int return code. Negative is failure, 1 is correct
 */
int SerialReaderThread::createSerialPort()
{
    /* Open the serial port. Serail port property gets set by SerialReaderThread Object */
    m_serialPort = new QSerialPort;
    m_serialPort->setPortName(m_serialPortName);

    if (!m_serialPort->open(QIODevice::ReadWrite)) {
        qDebug() << QObject::tr("Failed to open port %1, error: %2").arg(m_serialPortName).arg(m_serialPort->errorString()) << endl;
        return -1;
    }

    m_serialPort->setBaudRate(115200, QSerialPort::Input); 
    m_serialPort->setDataBits(QSerialPort::Data8);
    m_serialPort->setParity(QSerialPort::NoParity);
    m_serialPort->setStopBits(QSerialPort::OneStop);
    m_serialPort->setFlowControl(QSerialPort::NoFlowControl);

    qDebug() << "Serial port opened successfully";
    return 0;
}


bool SerialReaderThread::parseData(QByteArray ba, mmwave_header_s header) 
{
    const uint LEN_TLV_HEADER = 8;
    uint index = 0;
    uint32_t num_tlvs = charArrayToInt(header.tlv_no);

    // qDebug() << "Looking for " << num_tlvs << "TLVs in [" << ba.length() << "] byte frame";

    for (int tlv_n = 0; tlv_n < num_tlvs && index < ba.length()-LEN_TLV_HEADER; tlv_n++)
    {
        int32_t tlv_value = -1;
        int32_t tlv_len = 0;
        do 
        {
            QByteArray tlv_header = ba.mid(index, LEN_TLV_HEADER);
            tlv_value = charArrayToInt(tlv_header.mid(0,4).data());
            tlv_len = charArrayToInt(tlv_header.mid(4,4).data());

            index++;
        } while ((!isValidTLVType(tlv_value) || (tlv_len < 0 || tlv_len > ba.length())) && index < ba.length()-LEN_TLV_HEADER);
            //keep looking for TLV header while tlv_value invalid OR length invalid AND Still-data-in-buffer
        index += (LEN_TLV_HEADER-1); //-1 due to index++ above

        handleTLV(tlv_value, tlv_len, ba.mid(index, tlv_len));

        // qDebug() << "Found TLV of type [" << tlv_value << "] and length: " << tlv_len;

        index += tlv_len;
    }

    return false;
}

/**
 * TODO: document this
 * 
 */
void SerialReaderThread::handleTLV(int tlv_value, int tlv_len, QByteArray ba)
{
    static int num_PCs = 0;
    
    switch (tlv_value) 
    {
        case MMWDEMO_OUTPUT_MSG_DETECTED_POINTS:
        {
            //format and send all the points in a vector
            std::vector<mmwave_point_t> points;

            // int num_points = tlv_len / sizeof(mmwave_point_t);
            // points.resize(num_points);
            for (int index = 0; index < tlv_len; index += sizeof(mmwave_point_t))
            {
                float x, y, z, dopp;
                char* point_data;
                mmwave_point_t new_point;

                //pull data from the byte array. Convert to character array and reinterpret bytes as float
                point_data = ba.mid(index, sizeof(mmwave_point_t)).data();
                x = *((float*) point_data);
                y = *((float*) (point_data + sizeof(float)));
                z = *((float*) (point_data + sizeof(float) * 2));
                dopp = *((float*) (point_data + sizeof(float) * 3));


                new_point.x = x - RADAR_OFFSET_X_M;
                new_point.y = y - RADAR_OFFSET_Y_M;
                new_point.z = z - RADAR_OFFSET_Z_M;
                new_point.doppler = dopp;

                char point_str[64];
                // sprintf(point_str, "New point in reader (x,y,z): (%0.5f, %0.5f, %0.5f)", new_point.x, new_point.y, new_point.z);
                // qDebug() << point_str;
                points.push_back(new_point);

            }

            // qDebug() << num_PCs << "th frame from reader";
            // for (mmwave_point_t point : points) {
            //     char point_str[64];
            //     // sprintf(point_str, "point in reader (x,y,z): (%0.5f, %0.5f, %0.5f)", point.x, point.y, point.z);
            //     // qDebug() << point_str;
                

            // }
            //send vector to thread to draw points onto frame
            sendPoints(points, num_PCs);

            num_PCs ++;
            break;
        }
        case MMWDEMO_OUTPUT_MSG_ENHANCED_PRESENCE_DETECTED:
        {
            //if 1, send an active state message to the other thread; need some low-pass filtering and repeat rejecti
            bool presence_detected;
            
            presence_detected = (bool) (*(ba.mid(0, sizeof(bool)).data()));

            // qDebug() << "Presence detected: " << presence_detected;

            // //do filtering and potentially send
            if (!SLEEP_DISABLED)
            {
                emit sendActiveState(presence_detected);
            }


            break;
        }
    }

    //send
}


/**
 * @brief Set the main run-loop of the thread to exit
 * 
 */
void SerialReaderThread::handleQuit()
{
    m_runSerialThread = false;
}

/**
 * @brief The top level function of the serial reader. This is automatically called when the thread is started, and will continuously read data over serial from the mmwave sensor.
 * 
 */
void SerialReaderThread::run() {
    printf("inside serialthread run\n");
    //Need to create serialport in this thread else QT application throws error.
    // QT doesn't allow QSocket to be accessed from two different threads (main thread and serial thread)
    // Get inside while loop and attempt to read the bytes only if the serial port was opened successfully
    char magic_word[8] = {0x02, 0x01, 0x04, 0x03, 0x06, 0x05, 0x08, 0x07};

    if(createSerialPort() >= 0) {


        while(m_runSerialThread == true) {
            if ( m_serialPort->bytesAvailable() > 0 || m_serialPort->waitForReadyRead(100)) {
                QByteArray ba;
                qDebug() << "search for next frame";
                if (readUntilPreamble(m_serialPort))
                {
                    QByteArray ba_tlvs, ba_header;
                    mmwave_header_s header;
                    // qDebug() << "Found preamble";
                    ba_header.append(magic_word, sizeof(magic_word));
                    while (ba_header.length() < sizeof(header))
                    {
                         if (( m_serialPort->bytesAvailable() > 0 || m_serialPort->waitForReadyRead(100) ) && ba_header.length() < sizeof(header)) {
                            ba = m_serialPort->read(sizeof(header) - ba_header.length());
                            ba_header.append(ba);
                        }
                    }
                    // qDebug() << "Found header";
                    memcpy(&header, ba_header.data(), sizeof(header));
                    // printStringAsHex(ba_header.data(), sizeof(header));

                    uint frame_length = charArrayToInt(header.packet_length) - sizeof(header);

                    // ba_tlvs.append(ba.mid(sizeof(header)));

                    // qDebug() << "Full length of frame should be: " << frame_length;

                    while (ba_tlvs.length() < (frame_length))
                    {
                        if (( m_serialPort->bytesAvailable() > 0 || m_serialPort->waitForReadyRead(100) ) && ba_tlvs.length() < frame_length) {
                            ba = m_serialPort->read(frame_length - ba_tlvs.length());
                            ba_tlvs.append(ba);
                        }

                    }
                    // qDebug() << "Found frame";
                    // printQStringHex(ba_tlvs);

                    parseData(ba_tlvs, header);

                }

            }
        }
    }
    else {
        while(true) {
            std::string input;
            qDebug() << "input please! Non-zero will turn on screen, 0 turns off: ";
            std::cin >> input;
            if (input.compare("0") == 0)
            {
                qDebug("it was a zero..");
                emit sendActiveState(false);
            }
            else {
                emit sendActiveState(true);
            }
        }
    }
    emit srtQuit();
    qDebug() << "Serial reader thread quit\n";
}

# Edge AI & mmWave Radar Doorbell Demo


This Demo combines vision and radar inputs to create an intelligent doorbell 
application, which activates camera and vision processing once motion is 
detected within a configurable region in front of the radar sensor. 

Since TI's radar sensors are robust to all lighting conditions, can determine 
per-point motion, and can measure distance, they are far more precise at 
detecting wakeupevents than PIR sensors. The IWRL6432 is a low power variant 
that operates in the low mW range, which is similar to the deepest sleep 
states of AM62x.

The software stack in this demo features Qt for graphics and OpenCV for image 
processing and face detection. There is a distinct version of this demo that 
replaces face detection with people detection using Deep Learning software 
from Plumerai. To receive access to this version, please contact 
[Plumerai](https://www.ti.com/tool/PLMR-3P-PEODET) and a TI engineer 
through [e2e forums](https://e2e.ti.com).


##Necessary Hardware

This demo requires several hardware components alongside the 
[AM62 Starter Kit (SK-AM62) EVM](https://www.ti.com/tool/SK-AM62):

* Camera capable of taking 1280 x 720 images. This demo used a OV5640 CSI2
  camera (PiCam v1) connected through CSI2 - please follow 
  [Academy instructions](https://dev.ti.com/tirex/explore/node?a=VLyFKFf__4.12.0&node=A__AIwN3jgf3NkdPtMYdoNKKw__linux_academy_am62x__XaWts8R__LATEST)__ 
  to set up. 
  USB Cameras will also work and require less manual setup.

* mmWave Radar EVM [IWRL6432-EVM](https://www.ti.com/product/IWRL6432) 

  * USB-micro to USB-A cable for serial connection

* Monitor capable of 1920 x 1080 resolution with HDMI input

* A host machine running Linux (preferably an Ubuntu LTS distribution, 
  18.04 or newer) for building the image.

* 2 female-to-male jumper wires for serial connection if using PC-visualization tool
  
  * Note that the visualization tool is only supported on Windows


##System Setup

To set up the AM62X-SK, follow the guides in [Evaluating Linux](https://dev.ti.com/tirex/explore/node?node=A__AOAGBjTSmmQDAVBns9xcNQ__linux_academy_am62x__XaWts8R__8.3.0.0%20v1) 
until the network is accessible from a Linux PC host.


### Host Machine SDK Setup

To build applications for the AM62, set up the Processor SDK Linux on a Linux 
PC host. Download a setup the SDK installation script from the 
[AM62 SDK downloads page](https://www.ti.com/tool/download/PROCESSOR-SDK-LINUX-AM62X). 
Ensure the version of this matches the WIC image flashed to the SD card. Note 
that the SDK includes the same WIC images under the ``filesystem`` directory.
Once complete, open a terminal and navigate to the installation directory. To 
build an application for SK-AM62, set up the environment by running: 

```
    cd [SDK_PATH]/arago
    source ./environment-setup
```

### mmWave Radar Setup


This demo leverages an existing TI Design on 
[mmWave Radar](https://dev.ti.com/tirex/explore/node?a=VLyFKFf__4.12.0&node=A__AO51zs59SvlY21oeAv7M.w__radar_toolbox__1AslXXD__LATEST) 
for doorbell and intelligent proximity detection.

Follow instructions on the page above for the doorbell application, but **please use the appimage binary under the radar directory**, which contains additional output to signal when there is motion within the proximity region. Use the instructions on the mmWave doorbell page for how to load this firmware onto the  mmWave sensor. 

Once the board is flashed and the boot pins are set back into functional mode, 
continue to the next step.

To start the radar, a configuration file must be sent. From the 'radar' directory, run:

```
    python setup_radar.py
```

This will use the .CFG file within that directory to configure the device. Please see that demo's page under Resource Explorer for details. Note that the region of motion for smart wakeup is configured with the line "presenceBoundaryBox" where the first six numbers set the dimensions for x (min, max), y, and z. 

## Building the Application on Linux PC Host


After ``source``-ing the environment, the current input line in the terminal 
will be preceded by ``[linux-devkit]``. 


Workaround for SDK version <= 08.05
-----------------------------------

If the SDK version is <= 08.05, then the mmwave_doorbell .pro file needs to 
be modified to point to a separate path containing the /usr directory that will 
be on the root filesystem of the AM62X. The sysroot is out of sync with the 
generated filesystem, and will otherwise cause linker errors for OpenCV. 

* Navigate to the SDK installation's ``bin`` folder

* Execute ``setup-targetfs-nfs.sh``. Note where this creates the filesystem 
  (which is identical to what the WIC image put onto the SD card)

* Copy this path, and add to the mmwavegesture_hmi.pro file to replace the 
  value for ``usr_path = /path/to/sysroot/or/rootfs/usr/`` with the path 
  to your own

SDK version 08.06.00.42 is missing OpenCV libraries, and this demo with not work
without them. Please use a major release <=8.5 or >8.6. 


### Making the Demo Binary

Make the demo binary within the root directory of the cloned repo:

.. code-block:: bash 
    
    qmake -config release
    make


## Running the Demo

The next steps are to connect additional hardware components, start the demo, 
and briefly explain its usage.

### Connecting and Positioning Components

Connect the Sitara EVM to the mmwave EVM with the USB to USB-micro cable. Connect HDMI and camera before plugging in power through USB-C. 

If using a CSI camera, this should be plugged in before powering on
the board. The first time it is used, the device tree overlay must be enabled before booting with "setenv name_overlays k3-am625-sk-csi2-ov5640.dtbo" and "saveenv" commands

<img src="doc/EVM-front.jpeg" width="300" />
<img src="doc/EVM-back.jpeg" width="300" />

The camera should be mounted near eye level. Face detection is most effective 
when looking at the camera head-on at a neutral angle. 

The mmWave sensor should be set up about a 4-5 ft (1.5m) from the ground, just
a few centimeters below the camera module for the most accurate results. Note that if the sensors are not close enough to each other and pointed at the same angle, projection of 3D points from radar onto the 2D image will be inaccurate.


### Starting the Demo

To run the demo, copy it over to the AM62X SK board. The assets/ and ui/ 
directories should also be copied over, as they contain important resources 
files for Qt. It is recommended to tarball the entire repo directory, transfer, 
and untarball. 

*  ``tar -cf demo.tar /path/to/demo/repo/*``
*  Transfer tarball to AM62 (copy to SD card or scp over network)
*  ``tar -xf demo.tar``

The demo will assume that the video feed is available through /dev/video0 and 
the mmWave Radar through /dev/ttyACM0. If a different port should be used, 
include ``-s /device/path/`` when calling the demo. 


Run using the default configuration ``./run_demo.sh``, or include additional
command line options  e.g., ``./run_demo.sh -s /device/path/``


    * ``-platform eglfs`` can be omitted if the window manager is left running, 
      although the application is designed to run full-screen
    * ``-s /dev/MMWAVE_DEVICE_NAME`` is the name of the serial port for the 
      *data* port coming off the mmWave sensor. This is typically the second port
      enumerated
    * ``-h`` to see all additional options



### Demo Usage

The radar sensor detects motion and will trigger if a user is within the configurable space (set using the CFG file under ./radar directory). When motion is detected here, the sensor sends a wakeup signal to the AM62x processor. The processor is not fully set into sleep mode for this demo, but to instead show an alternate screen indicating sleep. When woken up, the screen will activate to show the camera feed with vision analytics. When there is no motion detected in the region again, the processor will go back to its idle state.

Vision processing is using OpenCV for standard head/face detection, but more complex models are possible, such as Multicoreware's face recognition or Plumerai's people detection. Please contact your local field team or post on TI's e2e forums to learn more about these.

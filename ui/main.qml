/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

import QtQuick 2.14
import QtQuick.Window 2.14
import QtMultimedia 5.14

import com.tihmi.classes 1.0


Window{
    id:mainWindow
    objectName: "mainWindow"
    visible: true
    width: 1920
    height: 1080
    title: qsTr("mmWave + Sitara Smart Doorbell Demo")
    color: "#202020"
    property bool activescreen: false
    property bool displaypoints: true

    Rectangle {
        id: lowpowerscreen
        visible: !activescreen
        height: parent.height
        width: parent.width
        color: "#202020"
    }
    
    Text {
        visible: true
        id: title
        objectName: "title"
        text: "IWRL6432 Low Power Radar + AM62X Doorbell Demo"
        font.family: "Helvetica"
        font.pixelSize: 70
        font.bold: true
        font.underline: true
        fontSizeMode: Text.Fit
        width: mainWindow.width*0.8
        height: mainWindow.height*0.0675
        color: "white"
        x: mainWindow.width/2 - width/2
        y: 20
        horizontalAlignment: Text.AlignHCenter

    }



    Image {
        id: tilogo
        visible: activescreen
        // x: mainWindow.width - 300
        // y: mainWindow.height - 50
        anchors.left: parent.left
        anchors.bottom: facedetectvideo.bottom
        anchors.leftMargin: 10
        width: parent.width - facedetectvideo.width - 2.5*anchors.leftMargin
        fillMode: Image.PreserveAspectFit
        source: "../assets/TI-logo.png"
    }



    Text {
        visible: !activescreen
        text: "Low Power Mode\n\nStep Closer to Turn On"
        color: "white"
        font.family : "Monospace"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        font.pixelSize: 70
        horizontalAlignment: Text.AlignHCenter

    }


    Camera {
        id: camera

        viewfinder.resolution: Qt.size(1280, 720)

        imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash

        exposure {
            exposureCompensation: -1.0
            exposureMode: Camera.ExposurePortrait
        }

        flash.mode: Camera.FlashRedEyeReduction

    }

    FaceDetectFilter{
        id: faceDetectFilter
        objectName: "faceDetectFilter"
    }

    VideoOutput {
        id: facedetectvideo
        visible: activescreen
        source: camera
        focus : visible // to receive focus and capture key events when visible
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.verticalCenter: parent.verticalCenter
        height: 720 * 1.18
        width: 1280 * 1.18
        filters: [ faceDetectFilter ]
    }

    Text {
        id: powerreadings
        objectName: "PowerReadingsText"
        text: "Power Mode:\tSLEEP\nSitara:\t\t10 mW\nRadar:\t\t5 mW"
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: facedetectvideo.top
        font.pixelSize: 40
        width: parent.width * 0.3
        height: font.pixelSize * 3.25
        fontSizeMode: Text.Fit
        color: "white"
    }



    Text {
        visible: activescreen
        id: fps
        objectName: "FPSText"
        anchors.right:facedetectvideo.right
        anchors.left: powerreadings.left
        anchors.leftMargin: 5
        anchors.top: powerreadings.bottom
        anchors.topMargin: 10
        text: "Face-detect Perf: NA"
        font.family: "Helvetica"
        fontSizeMode: Text.Fit
        font.pixelSize: 36
        color: "white"
    }

    Item {
        id: pointcloudkey
        visible: activescreen && displaypoints
        anchors.right: facedetectvideo.left
        anchors.rightMargin: 10
        width: parent.width - facedetectvideo.width - 2* anchors.rightMargin
        anchors.top: fps.bottom
        anchors.topMargin: 50
        height: parent.height * 0.2

        Text {
            id: pctext
            text: "Point Cloud Key"
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.topMargin: 10
            width: parent.width 
            font.family: "Helvetica"
            fontSizeMode: Text.Fit
            font.pixelSize: 36
            color: "white"
            horizontalAlignment: Text.AlignHCenter

        }


        Item {
            id: closerpoint
            height:parent.height
            width: parent.width*0.4
            anchors.left: parent.left
            anchors.leftMargin: 30

            Text {
                id:closertext
                anchors.horizontalCenter: parent.horizontalCenter
                y: parent.height * 0.35
                text: "Closer"
                font.family: "Helvetica"
                fontSizeMode: Text.Fit
                font.pixelSize: 22
                color: "white"
                horizontalAlignment: Text.AlignHCenter

            }


            Rectangle {
                id: smallcircleas
                width: 60
                y: parent.height * 0.5          
                height: width
                color: "#808080"
                border.color: "white"
                border.width: 2
                radius: width*0.5
                anchors.horizontalCenter: parent.horizontalCenter

            }
        }


        Item {
            id: furtherpoint
            height:parent.height
            width: parent.width * 0.4
            anchors.left: closerpoint.right

            Text {
                id:furthertext
                anchors.horizontalCenter: parent.horizontalCenter
                y: parent.height * 0.35
                text: "Further"
                font.family: "Helvetica"
                fontSizeMode: Text.Fit
                font.pixelSize: 22
                color: "white"
                horizontalAlignment: Text.AlignHCenter

            }

            Rectangle {
                id: furtherpointcicle
                width: 30
                height: width
                y: parent.height * 0.5
                color: "#808080"
                border.color: "white"
                border.width: 2
                radius: width*0.5
                anchors.horizontalCenter: parent.horizontalCenter

            }
        }


    }

}

import serial, time
from pprint import pprint

cfg=None
cfg_name = 'CES_DEMO_point_filtering_robust_major_motion_upside_down.cfg'
with open(cfg_name, 'r') as cfg_file:
    cfg = cfg_file.readlines()

pprint(cfg)
print(len(cfg))

cliCom = serial.Serial('/dev/ttyACM0', 115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,timeout=0.3)

for i,line in enumerate(cfg):
    print("writing line %d" % i)
    print("line: %s" % line)
    time.sleep(.03)
    cliCom.write(line.encode())
    ack = cliCom.readline()
    print(ack)
    ack = cliCom.readline()
    print(ack)

print("Done sending lines")
time.sleep(1)
QT += quick serialport core multimedia

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ./include

SOURCES += src/main.cpp src/serialreaderthread.cpp src/face_detect.cpp  

HEADERS +=  include/serialreaderthread.h include/face_detect.h  

CONFIG(debug, debug|release) {
        unix: DEFINES += DEBUG
        build_dir = build/debug
} else {
        unix: DEFINES += RELEASE
        build_dir = build/release
}

DESTDIR = $${build_dir}/
OBJECTS_DIR = $${build_dir}/obj
MOC_DIR = $${build_dir}/moc
UI_DIR = ui

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH = ui

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

CXXFLAGS +=  -ldl -lz -lutil
CFLAGS += -ldl -lutil
LDFLAGS += -Wl --copy-dt-needed-entries -ldl

DISTFILES +=

unix: LIBS += -lopencv_gapi  -lopencv_rgbd -lopencv_video -lopencv_xfeatures2d -lopencv_shape -lopencv_ml -lopencv_ximgproc -lopencv_objdetect -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_flann -lopencv_xphoto -lopencv_photo -lopencv_imgproc -lopencv_core -ldl
unix: LIBS += -L./lib 

#UPDATE!
usr_path = /home/reese/3-sdks/1-am62/08.04.01.03/targetNFS/usr

unix: INCLUDEPATH += $${usr_path}/include/opencv4
unix: LIBS += -L$${usr_path}/lib 
unix: QMAKE_RPATHLINKDIR += $${usr_path}/lib

